//
//  FCProfile.swift
//  lupton-showcase
//
//  Created by Shane Lupton on 3/24/16.
//  Copyright © 2016 Shane Lupton. All rights reserved.
//

import Foundation
import FBSDKCoreKit
import FBSDKLoginKit

let profile = FBSDKProfile!
    
/*!
 @abstract initializes a new instance.
 @param userID the user ID
 @param firstName the user's first name
 @param middleName the user's middle name
 @param lastName the user's last name
 @param name the user's complete name
 @param linkURL the link for this profile
 @param refreshDate the optional date this profile was fetched. Defaults to [NSDate date].
 */
    
    pro
- (instancetype)initWithUserID:(NSString *)userID
firstName:(NSString *)firstName
middleName:(NSString *)middleName
lastName:(NSString *)lastName
name:(NSString *)name
linkURL:(NSURL *)linkURL
refreshDate:(NSDate *)refreshDate NS_DESIGNATED_INITIALIZER;


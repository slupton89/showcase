//
//  DataService.swift
//  lupton-showcase
//
//  Created by Shane Lupton on 3/5/16.
//  Copyright © 2016 Shane Lupton. All rights reserved.
//

import Foundation
import Firebase
import FBSDKCoreKit


let URL_BASE = "https://sweltering-torch-4874.firebaseIO.com"
//let URL_FBPIC = "http://graph.facebook.com//picture?type=large"

class DataService {
    static let ds = DataService()
    
    private var _REF_BASE = Firebase(url: "\(URL_BASE)")
    private var _REF_POSTS = Firebase(url: "\(URL_BASE)/posts")
    private var _REF_USERS = Firebase(url: "\(URL_BASE)/users")
    
    var REF_BASE: Firebase {
        return _REF_BASE
    }
    
    var REF_POSTS: Firebase {
        return _REF_POSTS
    }
    
    var REF_USERS: Firebase {
        return _REF_USERS
    }

    
    var REF_USER_CURRENT: Firebase {
        let uid = NSUserDefaults.standardUserDefaults().valueForKey(KEY_UID) as! String
        let user = Firebase(url: "\(URL_BASE)").childByAppendingPath("users").childByAppendingPath(uid)
        return user!
    }
    
    func createFirebaseUser(uid: String, user: Dictionary<String, String>) {
        REF_USERS.childByAppendingPath(uid).setValue(user)
    }
    

    
    
    
}
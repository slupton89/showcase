//
//  Post.swift
//  lupton-showcase
//
//  Created by Shane Lupton on 3/23/16.
//  Copyright © 2016 Shane Lupton. All rights reserved.
//

import Foundation
import Firebase

class Post {
    private var _postDescription: String!
    private var _imageUrl: String?
    private var _likes: Int!
    private var _userName: String!
    private var _postKey: String!
    private var _postRef: Firebase!
    private var _postPic: String?
    private var _timestamp: String!
    
    var postDescription: String {
        return _postDescription
    }
    
    var imageUrl: String? {
        return _imageUrl
    }
    
    var likes: Int {
        return _likes
    }
    
    var userName: String {
        return _userName
    }
    
    var postKey: String {
        return _postKey
    }
    
    var postPic: String? {
        return _postPic
    }
    
    var timestamp: String {
        return _timestamp
    }
    
    init(description: String, imageUrl: String?, username: String, pic: String, timestamp: String) {
        self._postDescription = description
        self._imageUrl = imageUrl
        self._userName = userName
        self._postPic = pic
        self._timestamp = timestamp
    }
    
    init(postKey: String, dictionary: Dictionary<String, AnyObject>) {
        self._postKey = postKey
        
        if let likes = dictionary["likes"] as? Int {
            self._likes = likes
        }
        
        if let imgUrl = dictionary["imageUrl"] as? String {
            self._imageUrl = imgUrl
        }
        
        if let desc = dictionary["description"] as? String {
            self._postDescription = desc
        }
        
        if let proPic = dictionary["propic"] as? String {
            self._postPic = proPic
        }
        
        if let userName = dictionary["userName"] as? String {
            self._userName = userName
        }
        
        if let timestamp = dictionary["timestamp"] as? String {
            self._timestamp = timestamp
        }
        
        self._postRef = DataService.ds.REF_POSTS.childByAppendingPath(self._postKey)
    }
    
    func adjustLikes(addLike: Bool) {
        
        if addLike {
            _likes = _likes + 1
        } else {
            _likes = _likes - 1
        }
        
        _postRef.childByAppendingPath("likes").setValue(_likes)
        
    }
    
}

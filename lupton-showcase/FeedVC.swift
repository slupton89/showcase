//
//  FeedVC.swift
//  lupton-showcase
//
//  Created by Shane Lupton on 3/23/16.
//  Copyright © 2016 Shane Lupton. All rights reserved.
//

import UIKit
import Firebase
import Alamofire

class FeedVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var postField: MaterialTextField!
    @IBOutlet weak var imageSelectorImg: UIImageView!

    var characters: String!
    var reverseTimestamp: String!
    var posts = [Post]()
    var imageSelected = false
    var imagePicker: UIImagePickerController!
    var proPicUrl: String!
    var profileUrl: String!
    
    static var imageCache = NSCache()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 355
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        DataService.ds.REF_POSTS.queryOrderedByChild("timestamp").observeEventType(.Value, withBlock: { snapshot in
        
            self.posts = []
            if let snapshots = snapshot.children.allObjects as? [FDataSnapshot] {
                
                for snap in snapshots {
                    
                    if let postDict = snap.value as? Dictionary<String, AnyObject> {
                        let key = snap.key
                        let post = Post(postKey: key, dictionary: postDict)
                        self.posts.append(post)
                    }
                }
                
            }
            self.tableView.reloadData()
        })
        
    }
    
    override func viewDidAppear(animated: Bool) {
        getProfileUrl()
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let post = posts[indexPath.row]
        
        if let cell = tableView.dequeueReusableCellWithIdentifier("PostCell") as? PostCell {
            
            cell.request?.cancel()
            
            var img: UIImage?
            
            var proPicImg: UIImage!
            
            if let url = post.imageUrl {
                img = FeedVC.imageCache.objectForKey(url) as? UIImage
            } 
            
            cell.configureCell(post, img: img, proPic: proPicImg)
            
            return cell
        } else {
            return PostCell()
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let post = posts[indexPath.row]
        
        if post.imageUrl == nil {
            return 150
        } else {
            return tableView.estimatedRowHeight
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        imageSelectorImg.image = image
        imageSelected = true
    }

    @IBAction func selectImage(sender: AnyObject) {
        presentViewController(imagePicker, animated: true, completion: nil)
    }

    @IBAction func makePost(sender: AnyObject) {
        
        if let txt = postField.text where txt != "" {
            
            if let img = imageSelectorImg.image where imageSelected == true {
                let urlStr = "https://post.imageshack.us/upload_api.php"
                let url = NSURL(string: urlStr)!
                let imgData = UIImageJPEGRepresentation(img, 0.2)!
                let keyData = "12DJKPSU5fc3afbd01b1630cc718cae3043220f3".dataUsingEncoding(NSUTF8StringEncoding)!
                let keyJSON = "json".dataUsingEncoding(NSUTF8StringEncoding)!
                
                
                Alamofire.upload(.POST, url, multipartFormData: { multipartFormData in
                    
                    multipartFormData.appendBodyPart(data: imgData, name: "fileupload", fileName: "image", mimeType: "image/jpg")
                    multipartFormData.appendBodyPart(data: keyData, name: "key")
                    multipartFormData.appendBodyPart(data: keyJSON, name: "format")
                    
                }) { encodingResult in
                    
                    switch encodingResult {
                    case .Success(let upload, _, _):
                        upload.responseJSON(completionHandler: { response in
                            if let info = response.result.value as? Dictionary<String, AnyObject> {
                                if let links = info["links"] as? Dictionary<String, AnyObject> {
                                    if let imgLink = links["image_link"] as? String {
                                        //print("LINK: \(imgLink)")
                                        self.postToFirebase(imgLink)
                                    }
                                }
                            }
                        })
                    case .Failure(let error):
                        print(error)
                    }
                    
                }
                
            } else {
                self.postToFirebase(nil)
            }
        }
    }
    
    func postToFirebase(imgUrl: String?) {
        
        if DataService.ds.REF_USER_CURRENT.authData.provider == "facebook" {

        var post: Dictionary<String, AnyObject> = ["description": postField.text!, "likes": 0, "propic": profileUrl, "ownerID" : DataService.ds.REF_USER_CURRENT.authData.uid, "userName": DataService.ds.REF_USER_CURRENT.authData.providerData["displayName"]!, "timestamp": FirebaseServerValue.timestamp()]
        
        
        if imgUrl != nil {
            post["imageUrl"] = imgUrl!
        }
        
        let firebasePost = DataService.ds.REF_POSTS.childByAutoId()
        firebasePost.setValue(post)
        
        postField.text = ""
        imageSelectorImg.image = UIImage(named: "camera")
        imageSelected = false
    

        //print("PIC URL: \(proPicUrl)")
        
        tableView.reloadData()
            
        }else {
            var post: Dictionary<String, AnyObject> = ["description": postField.text!, "likes": 0, "ownerID" : DataService.ds.REF_USER_CURRENT.authData.uid, "userName": DataService.ds.REF_USER_CURRENT.authData.providerData["email"]! ]
            
            
            if imgUrl != nil {
                post["imageUrl"] = imgUrl!
            }
            
            let firebasePost = DataService.ds.REF_POSTS.childByAutoId()
            firebasePost.setValue(post)
            
            postField.text = ""
            imageSelectorImg.image = UIImage(named: "camera")
            imageSelected = false
            
            
            //print("PIC URL: \(proPicUrl)")
            
            tableView.reloadData()
        }
    }
    
    
    func getProfileUrl() {
        
        if DataService.ds.REF_USER_CURRENT.authData.provider == "facebook" {
            
        let proPicGet: String = "https://graph.facebook.com/\(DataService.ds.REF_POSTS.authData.uid)/picture?type=large"
        let proPicUrl = proPicGet.stringByReplacingOccurrencesOfString("facebook:", withString: "")
        
        profileUrl = "\(proPicUrl)"
            
        } else {
            profileUrl = ""
        }
    }
    
    
}









